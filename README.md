# ThomOS

thomos is the switchblade operating system.  It abides by the KISS philosophy and ships with thom-utils.

#### Download ThomOS: https://sourceforge.net/projects/thomos/files/

# See also:

thom-utils: minimalist script set: https://gitlab.com/souperdoupe/thom-utils

crunkbong (predecessor): https://souperdoupe.github.io
